A bash (and soon other) completion for LIGO channel names.

This provides a bash completion script along with a channel completion program.
A set of channel databases are required.  Each database is a simple flat text file where
every line is a single channel name.  The file must be sorted.

The path to the files is set via the 'EPICS_CHAN_LIST' and 'NDS_CHAN_LIST' environment variables.

As the names imply the database are for EPICS channels or DAQ/NDS channels.  This split is needed
as in the LIGO control room there are > 500k channels, with roughly half are available only as EPICS channels.
By specifically supporting EPICS channels then non-daq channels like string records can be completed against
for caget/caput/...

If you have a list of channels you can have the application sort it for you.  Please note that
it is the bash scripts that use the EPICS_CHAN_LIST/NDS_CHAN_LIST variables, the completion engine
directly takes the database on the command line.

<pre>
# assuming that the channel database is
# located at db.txt.
ligo_channel_completion -d db.txt -r > tmpfile
mv tmpfile db.txt
</pre>

Requirements:
 * A C++11 compliant compiler.
 * CMake 3+
 * Boost iterator library
 * pstreams library

Debian requirements:
 * build-essential
 * bash-completion
 * libboost-dev
 * libpstreams-dev
 
Tested on:
 * Debian 8 & 9
 
Building:
 * create a build area
 * run cmake
 * install
 
Example (starting from the source directory)

<pre>
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
make install
</pre>

Components installed (Assuming a install prefix of /usr):
 * a binary ligo_channel_completion to /usr/lib
 * a set shell script to the base completion libraries at /usr/share/bash-completion/completions

After the install is successful, start a new bash shell and use the channel completion and make
sure that the EPICS_CHAN_LIST and NDS_CHAN_LIST environment variables are exported and refer
to proper databases.

<pre>
$ ndscope H&lt;tab&gt;
H0:FMC  H1:AOS  H1:CDS  H1:HPI  H1:ISI  H1:OMC  H1:SQZ  H1:VID  
H0:SAF  H1:ASC  H1:DAQ  H1:IMC  H1:LSC  H1:PEM  H1:SUS  
H0:VAC  H1:AWC  H1:FEC  H1:IOP  H1:OAF  H1:PSL  H1:SYS  
H1:ALS  H1:CAL  H1:GRD  H1:ISC  H1:ODC  H1:SEI  H1:TCS
</pre>