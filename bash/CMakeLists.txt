# Add commands that can use LIGO channel completion to this list
set (COMPLETE_THESE_NDS_COMMANDS
    ndscope
)

set (COMPLETE_THESE_EPICS_COMMANDS
    cdsutils
    caget
    caput
    camonitor
    probe
)

set(COMPLETE_NDS_COMMAND_REGISTRATION)
foreach(entry ${COMPLETE_THESE_NDS_COMMANDS})
    set(COMPLETE_NDS_COMMAND_REGISTRATION "${COMPLETE_NDS_COMMAND_REGISTRATION}
complete -F _ligo_nds_channel_completion ${entry}")
endforeach()

set(COMPLETE_EPICS_COMMAND_REGISTRATION)
foreach(entry ${COMPLETE_THESE_EPICS_COMMANDS})
    set(COMPLETE_EPICS_COMMAND_REGISTRATION "${COMPLETE_EPICS_COMMAND_REGISTRATION}
complete -F _ligo_epics_channel_completion ${entry}")
endforeach()

set(COMPLETE_SCRIPT_DIR "${CMAKE_INSTALL_PREFIX}/share/bash-completion/completions")

configure_file(ligo_nds_channel_completion.in ${CMAKE_CURRENT_BINARY_DIR}/ligo_nds_channel_completion @ONLY)
configure_file(ligo_epics_channel_completion.in ${CMAKE_CURRENT_BINARY_DIR}/ligo_epics_channel_completion @ONLY)
configure_file(link_file.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/link_file.cmake @ONLY)

install(FILES "${CMAKE_CURRENT_BINARY_DIR}/ligo_nds_channel_completion"
        DESTINATION ${COMPLETE_SCRIPT_DIR})
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/ligo_epics_channel_completion"
        DESTINATION ${COMPLETE_SCRIPT_DIR})

install(SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/link_file.cmake)
