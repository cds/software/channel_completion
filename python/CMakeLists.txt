cmake_minimum_required(VERSION 3.0)
project(complete_chan_python)

MESSAGE("Starting config for ${PYTHON_EXECUTABLE}")

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../cmake;${CMAKE_MODULE_PATH}")

find_package(pybind11)
include(Cpp11)

enable_testing()
set(SKIP_TESTS 1)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/third_party)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

add_subdirectory(../third_party ${CMAKE_CURRENT_BINARY_DIR}/third_party)
add_subdirectory(../lib ${CMAKE_CURRENT_BINARY_DIR}/lib)

pybind11_add_module(ligo_channels MODULE  python_completion.cc)
target_link_libraries(ligo_channels PRIVATE libcompletion)

#--------------------------------------------------------------------
# Determine where packages should be installed
#--------------------------------------------------------------------
set( pyexecdir_script "from distutils.sysconfig import get_python_lib; print( get_python_lib(1,0,\"${CMAKE_INSTALL_PREFIX}\") );" )

execute_process(
        COMMAND ${PYTHON_EXECUTABLE} -c "${pyexecdir_script}"
        OUTPUT_VARIABLE pyexecdir
        OUTPUT_STRIP_TRAILING_WHITESPACE
)

install(TARGETS ligo_channels
        DESTINATION ${pyexecdir})

MESSAGE("Ending config for ${PYTHON_EXECUTABLE}")
