// this main function is almost entirely taken from
// https://github.com/catchorg/Catch2/blob/master/docs/own-main.md#top

// The additional changes are
// Copyright 2019 California Institute of Technology.

// You should have received a copy of the licensing terms for this
// software included in the file “LICENSE” located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt

#include <string>

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

std::string test_program_name;

int
main( int argc, char* argv[] )
{
    Catch::Session session; // There must be exactly one instance

    // Build a new parser on top of Catch's
    using namespace Catch::clara;
    auto cli = session.cli( ) // Get Catch's composite command line parser
        | Opt( test_program_name,
               "The program program to test" ) // bind variable to a new option,
              // with a hint string
              [ "--program" ] // the option
        // names it
        // will
        // respond to
        ( "The test program" ); // description string for the help output

    // Now pass the new composite back to Catch so it uses that
    session.cli( cli );

    // Let Catch (using Clara) parse the command line
    int returnCode = session.applyCommandLine( argc, argv );
    if ( returnCode != 0 ) // Indicates a command line error
        return returnCode;

    return session.run( );
}